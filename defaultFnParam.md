# Default Function Parameter

> In JavaScript function parameters default to **undefined** - MDN

If we want to assign default values to our parameters we can do so.
```js
const sumWithoutDefault = (a, b) => {
    return a + b;
}

console.log(sumWithoutDefault(2)); //-> NaN

const sumWithDefault = (a, b = 0) => {
    return a + b;
}

console.log(sumWithDefault(2)); //-> 2
```
In above example, if we have not assigned the default value to parameter in function `sumWithoutDefault`, it will return `NaN` because by default the function processes it as `sum` of first parameter `a = 2` and second parameter `b = undefined`, but in the second function `sumWithDefault` the parameter `b` gets assigned `0` as the default value, which results in `2 = 2 + 0`.

We can also assign default values to a variable if the argument object does not contain a property with that variable name

```js
const movieData = {
    filmName: "Matrix",
    releaseYear: 1999,
    genre: "Sci-Fi",
    leadCharacters: ["Trinity", "Neo", "Morpheus"],
}

const displayMovieInfo = ({filmName, releaseYear, platform = "Cinema"}) => {
    console.log( `${filmName} was released in ${releaseYear} on ${platform}`);
}

console.log(displayMovieInfo(movieData)); //-> Matrix was released in 1999 on Cinema
```

In the above example, the object `movieData` has no `platform` property so we are assigning it a default value of ` = "Cinema"`.

---
References
- [MDN article on default parameters](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Default_parameters)
- [Article for object default function parameters](https://gomakethings.com/destructuring-function-parameters-with-vanilla-js-for-better-developer-ergonomics/)